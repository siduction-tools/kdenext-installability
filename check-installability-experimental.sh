#!/bin/bash

#TMPDIR=`mktemp -d`

#cd $TMPDIR

# Packages foreground
if [ -f Packages_kdenext_experimental ]; then
	mv Packages_kdenext_experimental Packages
fi
wget -N http://packages.siduction.org/kdenext/dists/experimental/main/binary-amd64/Packages \
	>/dev/null 2>/dev/null
mv Packages Packages_kdenext_experimental
cp Packages_kdenext_experimental Packages_foreground


# Packages background

if [ -f Packages_debian_unstable.gz ]; then
	mv Packages_debian_unstable.gz Packages.gz
fi
wget -N http://ftp.debian.org/debian/dists/unstable/main/binary-amd64/Packages.gz \
	>/dev/null 2>/dev/null
mv Packages.gz Packages_debian_unstable.gz
zcat Packages_debian_unstable.gz > Packages_background

# Let's check
dose-debcheck -fe --fg=Packages_foreground --bg=Packages_background > results_experimental.txt
echo "" >> results_experimental.txt
date >> results_experimental.txt

#cd -

#rm -rf $TMPDIR
