#!/bin/bash

#TMPDIR=`mktemp -d`

#cd $TMPDIR

# Packages foreground
if [ -f Packages_kdenext_kde-frameworks-exp ]; then
	mv Packages_kdenext_kde-frameworks-exp Packages
fi
wget -N http://packages.siduction.org/kdenext/dists/kde-frameworks-exp/main/binary-amd64/Packages \
	>/dev/null 2>/dev/null
mv Packages Packages_kdenext_kde-frameworks-exp
cp Packages_kdenext_kde-frameworks-exp Packages_foreground


# Packages background

if [ -f Packages_debian_unstable.gz ]; then
	mv Packages_debian_unstable.gz Packages.gz
fi
wget -N http://ftp.debian.org/debian/dists/unstable/main/binary-amd64/Packages.gz \
	>/dev/null 2>/dev/null
mv Packages.gz Packages_debian_unstable.gz
zcat Packages_debian_unstable.gz > Packages_background

if [ -f Packages_kdenext_experimental ]; then
	mv Packages_kdenext_experimental Packages
fi
wget -N http://packages.siduction.org/kdenext/dists/experimental/main/binary-amd64/Packages \
	>/dev/null 2>/dev/null
mv Packages Packages_kdenext_experimental
cat Packages_kdenext_experimental >> Packages_background

# Let's check
dose-debcheck -fe --fg=Packages_foreground --bg=Packages_background > results_kde-frameworks-exp.txt
echo "" >> results_kde-frameworks-exp.txt
date >> results_kde-frameworks-exp.txt

#cd -

#rm -rf $TMPDIR
